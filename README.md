# Projekt

* [x] GIT repozitář na Gitlabu. Repozitář má README.md
* [ ] Použití libovolného generátoru statických stránek dle vlastního výběru.
* [ ] Vytvořená CI v repozitáři.
* [ ] CI má dvě úlohy: Test kvality Markdown. Generování HTML stránek z Markdown.
* [ ] CI má automatickou úlohou nasazení web stránek (deploy).
* [ ] Gitlab projekt má fukční web stránky s generovaným obsahem na URL:
